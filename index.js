module.exports = function(req,res,cb){
  if(req.method === 'POST'){  
    var body = '';
    req.setEncoding('utf8');
    req.on('data',function(data){
      body += data;
    });
    req.on('end', function(){
      console.log(req.body);
      req.body = JSON.parse(body);
      cb(req,res);      
    });
  }else{
    cb(req,res);
  }
};
